import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:upstreet_flutter_code_challenge/constants/APIConstants.dart';

class DioClient {
  static Dio _dio;

  static Dio getClient() {
    if (_dio == null) {
      BaseOptions options = new BaseOptions(
        baseUrl: APIConstants.API_URL,
        connectTimeout: 10000,
        receiveTimeout: 10000,
      );

      _dio = new Dio(options);
    }
    return _dio;
  }
}

void showErrorDialog(BuildContext context, dynamic errorResponse,
    {Function onOKPressed}) {
  String errorTitle = "", errorMsg = "", buttonText = "OK";
  bool dismissable = true;

  if (errorResponse is DioError) {
    if (errorResponse.error is SocketException) {
      errorTitle = "Network Error";
      errorMsg =
          "There is some issue with your internet connection. Please check your internet connection settings and try again.";
    } else {
      errorTitle = "Error";
      errorMsg = errorResponse.toString();
    }
  }
  showDialog(
      context: context,
      barrierDismissible: dismissable,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () {
            return Future.value(dismissable);
          },
          child: AlertDialog(
            title: Text(errorTitle),
            content: Text(errorMsg),
            actions: <Widget>[
              FlatButton(
                onPressed: (() {
                  Navigator.of(context).pop();
                  if (onOKPressed != null) {
                    onOKPressed();
                  }
                }),
                child: Text(buttonText),
              )
            ],
          ),
        );
      });
}
