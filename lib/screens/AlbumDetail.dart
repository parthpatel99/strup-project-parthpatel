import 'package:flutter/material.dart';
import 'package:upstreet_flutter_code_challenge/api/AlbumAPIs.dart';
import 'package:upstreet_flutter_code_challenge/api/DioClient.dart';
import 'package:upstreet_flutter_code_challenge/models/AlbumModel.dart';
import 'package:upstreet_flutter_code_challenge/widgets/ProgressContainerView.dart';

class AlbumDetail extends StatefulWidget {
  AlbumModel album;
  AlbumDetail(var args) {
    album = args;
  }
  @override
  _AlbumDetailState createState() => _AlbumDetailState();
}

class _AlbumDetailState extends State<AlbumDetail> {
  bool isProgressRunning = false;
  bool isUpdated = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.album.title),
        actions: [
          IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                deleteAlbum();
              })
        ],
      ),
      body: WillPopScope(
        onWillPop: () async {
          Navigator.of(context).pop(isUpdated);
          return true;
        },
        child: ProgressContainerView(
          isProgressRunning: isProgressRunning,
          child: Container(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.network(widget.album.url, width: 200, height: 200),
                SizedBox(height: 40),
                Text(widget.album.title,
                    style: Theme.of(context).textTheme.headline5)
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.edit, color: Colors.white),
          onPressed: () {
            Navigator.of(context)
                .pushNamed('/addedit', arguments: widget.album)
                .then((albumUpdated) {
              if (albumUpdated != null) {
                setState(() {
                  widget.album = albumUpdated;
                  isUpdated = true;
                });
              }
            });
          }),
    );
  }

  void deleteAlbum() {
    setState(() {
      isProgressRunning = true;
    });
    AlbumAPIs.deleteAlbum(widget.album)
        .then((updatedAlbum) {
          Navigator.of(context).pop(true);
        })
        .catchError((error) => showErrorDialog(context, error))
        .whenComplete(() => {
              setState(() {
                isProgressRunning = false;
              })
            });
  }
}
