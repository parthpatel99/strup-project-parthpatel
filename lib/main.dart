import 'package:flutter/material.dart';
import 'package:upstreet_flutter_code_challenge/screens/AlbumAddEdit.dart';
import 'package:upstreet_flutter_code_challenge/screens/AlbumList.dart';
import 'package:upstreet_flutter_code_challenge/screens/AlbumDetail.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Upstreet Flutter code challenge',
      theme: ThemeData(
        primaryColor: const Color(0xff01046d),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => AlbumList(),
        '/detail': (context) => AlbumDetail(ModalRoute.of(context).settings.arguments),
        '/addedit': (context) => AlbumAddEdit(ModalRoute.of(context).settings.arguments)
      },
    );
  }
}
