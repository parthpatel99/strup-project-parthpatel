import 'package:flutter/material.dart';
import 'package:upstreet_flutter_code_challenge/api/AlbumAPIs.dart';
import 'package:upstreet_flutter_code_challenge/api/DioClient.dart';
import 'package:upstreet_flutter_code_challenge/models/AlbumModel.dart';
import 'package:upstreet_flutter_code_challenge/widgets/ProgressContainerView.dart';

// TODO:
// 1. Create a list view to display the album data from the fetching function in `api.dart`
// 2. The item of the list should contain the album's thumbnail and title

class AlbumList extends StatefulWidget {
  @override
  _AlbumListState createState() => _AlbumListState();
}

class _AlbumListState extends State<AlbumList> {
  List<AlbumModel> albumList = [];
  bool isProgressRunning = true;

  @override
  void initState() {
    super.initState();
    getAlbumList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Album List'),
      ),
      body: ProgressContainerView(
        isProgressRunning: isProgressRunning,
        child: Container(
          child: ListView.builder(
            itemCount: albumList.length,
            itemBuilder: (context, i) => albumList[i].getWidget(context, onUpdate: () => getAlbumList()),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(child: Icon(Icons.add, color: Colors.white), onPressed: () { 
        Navigator.of(context).pushNamed('/addedit').then((albumUpdated) {
          getAlbumList();
        });
      })
    );
  }

  void getAlbumList() {
    setState(() {
      isProgressRunning = true;
    });
    AlbumAPIs.getAlbums()
        .then((list) => {albumList = list})
        .catchError((error) => showErrorDialog(context, error))
        .whenComplete(() => {
              setState(() {
                isProgressRunning = false;
              })
            });
  }
}
