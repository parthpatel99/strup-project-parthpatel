import 'package:flutter/material.dart';
import 'package:upstreet_flutter_code_challenge/api/AlbumAPIs.dart';
import 'package:upstreet_flutter_code_challenge/api/DioClient.dart';
import 'package:upstreet_flutter_code_challenge/models/AlbumModel.dart';
import 'package:upstreet_flutter_code_challenge/widgets/ProgressContainerView.dart';

class AlbumAddEdit extends StatefulWidget {
  AlbumModel album;
  bool isNew = true;
  AlbumAddEdit(var args) {
    if (args != null) {
      isNew = false;
      album = args;
    }
  }
  @override
  _AlbumAddEditState createState() => _AlbumAddEditState();
}

class _AlbumAddEditState extends State<AlbumAddEdit> {
  TextEditingController _titleController, _urlController;
  bool isProgressRunning = false;

  @override
  void initState() {
    super.initState();
    _titleController =
        new TextEditingController(text: widget.album?.title ?? '');
    _urlController = new TextEditingController(text: widget.album?.url ?? '');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            !widget.isNew ? "Edit ${widget.album.title}" : "Add Album"),
      ),
      body: ProgressContainerView(
        isProgressRunning: isProgressRunning,
        child: Container(
          padding: EdgeInsets.all(24),
          child: Column(
            children: [
              TextFormField(
                controller: _titleController,
                decoration: InputDecoration(labelText: "Title"),
              ),
              SizedBox(height: 40),
              TextFormField(
                controller: _urlController,
                decoration: InputDecoration(labelText: "URL"),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.done, color: Colors.white),
          onPressed: () {
            if (widget.album == null) {
              widget.album = new AlbumModel();
            }
            widget.album.title = _titleController.text;
            widget.album.url = _urlController.text;
            widget.album.thumbnailUrl = _urlController.text;
            addOrUpdateAlbum();
          }),
    );
  }

  void addOrUpdateAlbum() {
    setState(() {
      isProgressRunning = true;
    });
    (widget.isNew
            ? AlbumAPIs.addAlbum(widget.album)
            : AlbumAPIs.updateAlbum(widget.album))
        .then((updatedAlbum) {
          Navigator.of(context).pop(widget.album);
        })
        .catchError((error) => showErrorDialog(context, error))
        .whenComplete(() => {
              setState(() {
                isProgressRunning = false;
              })
            });
  }
}
