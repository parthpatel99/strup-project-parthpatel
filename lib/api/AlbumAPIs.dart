import 'package:upstreet_flutter_code_challenge/api/DioClient.dart';
import 'package:upstreet_flutter_code_challenge/models/AlbumModel.dart';

class AlbumAPIs {

  static Future<List<AlbumModel>> getAlbums() async {
    var resp = await DioClient.getClient().get("/albums");
    if(resp.statusCode == 200) {
      return resp.data.map<AlbumModel>((e) => AlbumModel.fromJson(e)).toList();
    } else {
      return [];
    }
  }
  
  static Future<AlbumModel> addAlbum(AlbumModel album) async {
    var resp = await DioClient.getClient().post("/albums", data: album.toJson());
    if(resp.statusCode == 200) {
      return AlbumModel.fromJson(resp.data);
    } else {
      return null;
    }
  }
  
  static Future<AlbumModel> updateAlbum(AlbumModel album) async {
    var resp = await DioClient.getClient().patch("/albums/${album.id}", data: album.toJson());
    if(resp.statusCode == 200) {
      return AlbumModel.fromJson(resp.data);
    } else {
      return null;
    }
  }

  static Future<bool> deleteAlbum(AlbumModel album) async {
    var resp = await DioClient.getClient().delete("/albums/${album.id}");
    if(resp.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}