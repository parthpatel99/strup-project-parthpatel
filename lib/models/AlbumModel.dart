import 'package:flutter/material.dart';

class AlbumModel {
  String id;
  String title;
  String url;
  String thumbnailUrl;

  AlbumModel({this.id, this.title, this.url, this.thumbnailUrl});

  factory AlbumModel.fromJson(Map<String, dynamic> json) {
    return AlbumModel(
        id: json['_id'],
        title: json['title'],
        url: json['url'],
        thumbnailUrl: json['thumbnailUrl']);
  }

  Map toJson() {
    return {
      '_id': id,
      'title': title,
      'url': url,
      'thumbnailUrl': thumbnailUrl
    };
  }

  Widget getWidget(BuildContext context, {VoidCallback onUpdate}) {
    return InkWell(
      onTap: () {
        Navigator.of(context).pushNamed('/detail', arguments: this).then((isDeleted) {
          if (isDeleted != null && isDeleted) {
            onUpdate();
          }
        });
      },
      child: Container(
        padding: EdgeInsets.all(12),
        child: Row(
          children: [
            Image.network(
              thumbnailUrl,
              width: 60,
              height: 60,
            ),
            SizedBox(width: 12),
            Expanded(
                child:
                    Text(title, style: Theme.of(context).textTheme.subtitle1))
          ],
        ),
      ),
    );
  }
}
