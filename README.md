APIs currently hosted at Heroku https://ptestapp123.herokuapp.com/

You can get API project at https://gitlab.com/parthpatel99/strup-project-parthpatel-backend

====
I have completed the challange as per you told, but due to limited time available and the APIs also needs to created by me i have done it as the simplest and fastest way possible, there will be large scope of improvement, for example we can add MobX for state management, and other design patterns, but since its a very small project, i dont think we need to go into deeper. we should definately use that in the actual big project.

I have added one more delete functionality bcz i think thats needed.

currently data is hosted at my personal monogodb atlas instance.

APIs have been created by me on the NodeJS using mongoose plugin to connect with the MongoDB atlas.

We have many thing left to do for improvement.

I have not used that placeholder json APIs, i have created my own from that format, using NodeJS and datasource is in MongoDB Atlas.

I have assumed that this is a practice small project so i not digg deeper.

Honestly, It taken me about an hour to complete the task including API development in NodeJS, but there were some issue while deploying the APIs to Heroku server, thats why it taken some time.

Scope of Improvement are MobX implementation, Store, Error handling, Search, Multiple Images, Comments, Likes and other features. 

However, this application not needs to be deployed in the app store or play store, but if you want to do it, you should ensure that the proper permissions are set in the Android Manifest and Info plist for iOS, then you need to follow below process for release

Pre check :
check permission in manifest and plist.
check package name in android and iOS Project
change release version code and name in pubspec yaml

iOS Release Process :
run "flutter build ios --release" 
open xcode
archive the project
validate the generated archive
distribute the generated archive
open appstore connect
add new version
select the build after processing
add release notes or screenshots and other details if the app is first release. and submit for review.

Android Release Process :
run "flutter build appbundle"
open play store console
upload bundle.
upload screenshots and other info if its new release or the release not if an update.
send it for review.


I will be happy to have a session with team.
